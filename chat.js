function checkboxChecked() {
    return document.getElementById("check").checked;
}

function notEmptyFields() {
    return document.getElementById("nick").value && document.getElementById("message").value;
}


function enableFields(isEnabled) {
    var isDisabled = !isEnabled;
    document.getElementById("chat").disabled = isDisabled;
    document.getElementById("nick").disabled = isDisabled;
    document.getElementById("message").disabled = isDisabled;
    document.getElementById("submitButton").disabled = isDisabled;
}

function update() {
    if (checkboxChecked()) {
        enableFields(true);
        getContent();
    } else {
        document.getElementById("chat").innerHTML = "";
        enableFields(false);
    }
}

function getTimestamp() {
    var now = new Date();
    var hours = ("0" + now.getHours()).slice(-2);
    var minutes = ("0" + now.getMinutes()).slice(-2);
    var seconds = ("0" + now.getSeconds()).slice(-2);
    var year = now.getFullYear();
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var day = ("0" + now.getDate()).slice(-2);

    var time = hours + ":" + minutes + ":" + seconds;
    var date = year + "-" + month + "-" + day;

    return date + " " + time;
}
function readRequest() {

    var http_request = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    }
    if (!http_request) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    http_request.onreadystatechange = function () {

        if (http_request.readyState == XMLHttpRequest.LOADING && http_request.status == 200) {
            if (checkboxChecked()) {
                document.getElementById("chat").innerHTML = "Loading...";
            }
        }
        if (http_request.readyState == XMLHttpRequest.DONE) {
            if (checkboxChecked()) {
                document.getElementById("chat").innerHTML = http_request.responseText;
                setTimeout(function () {
                    http_request.open("GET", "messages.php?timestamp=" + getTimestamp(), true);
                    http_request.send(null);
                }, 2000);
            }
        }
    };
    http_request.open('GET', "messages.php", true);
    http_request.send(null);
}

function send() {

    if (notEmptyFields()) {
        var nick = document.getElementById("nick").value;
        var message = document.getElementById("message").value;

        var queryString = { 'nick': nick, 'message': message };
        $.ajax(
            {
                type: 'GET',
                url: 'send.php',
                data: queryString,
                complete: function () {
                    document.getElementById("message").value = "";
                }
            }
        );
    }
}

function keyup(arg1) {
    if (arg1 == 13) send();
}


function getContent(timestamp) {
    var queryString = { 'timestamp': timestamp };
    $.ajax(
        {
            type: 'GET',
            url: 'messages.php',
            data: queryString,
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                document.getElementById("chat").innerHTML = obj.messages;
                getContent(obj.timestamp);
            }
        }
    );
}