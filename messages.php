<?php
$sourceFile = 'messages.txt';
while (true) {
    $lastAjaxCall = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

    clearstatcache();
    $lastDataFileModification = filemtime($sourceFile);

    if ($lastAjaxCall == null || $lastDataFileModification > $lastAjaxCall) {

        $data = file_get_contents($sourceFile);

        $result = array(
            'messages' => $data,
            'timestamp' => $lastDataFileModification
        );

        $json = json_encode($result);
        echo $json;

        break;

    } else {
        sleep( 1 );
        continue;
    }
}
?>
